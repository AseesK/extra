public class calculatorTest {
    private calculator calculator;

    @BeforeSuite
    public void SetUp(){
        calculator= new calculator();

    }
    @Test
    public void TestAdd(){
        Assert.assertEquals( calculator.add(4,5), 9);

    }
    @Test
    public void TestMul(){
        Assert.assertEquals(calculator.mul(2,8),16);
    }
    @Test
    public void TestSub(){
        Assert.assertEquals( calculator.sub(5,3), 2);

    }
    @Test
    public void TestDiv(){
        Assert.assertEquals( calculator.div(10,2),5);

    }

}


